class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
    	t.references :product_type, index: true
    	t.string :name , null: false 
      t.string :price, null: false 
      t.text :description, null: false 
      t.timestamps null: false
    end
  end
end

