class PageController < ApplicationController

  def index
  end

  def contact
    @current ||= current_user
  end	

	def about
	end	
	
  def sendEmail
  	redirect_to root_path , notice: contact_params[:email]
  end

private

def contact_params
	params.require(:page).permit(:name, :email, :message)
end	

end
